//increment
export function increment (index) {
	// body...
	return {
		type: 'INCREMENT_LIKES',
		index
	}
}

//add comment
export function addComment (postId,author,comment) {
	// body...
	console.log("Adding comment");
	return {
		type: 'ADD_COMMENT',
		postId,
		author,
		comment
	}
}

//remove comment
export function removeComment (postId,i) {
	// body...
	return {
		type: 'REMOVE_COMMENT',
		i,
		postId
	}
}