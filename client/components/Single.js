import React from 'react';
import Photo from './Photo.js';
import Comments from './Comments.js';

const Single = React.createClass({
	render() {
		const { postId } = this.props.params;
		const i = this.props.posts.findIndex((post) => post.code === postId);	
		const post = this.props.posts[i];
		const postsComments = this.props.comments[postId] || [];
		console.log(i);
	
		return (
			<div className="single-photo"> 
				<Photo post = {post} i = {i} {...this.props} />
				<Comments postsComments = {postsComments} {...this.props}/>
			</div>
		)
	}
});

export default Single;