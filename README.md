# React_Reduxstagram

A simple React + Redux implementation on static data of instagram. Implemented redux store, reducers to handle states of component at each level. Used Router to route between components. Developed reusable component of photo to be used in landing page as well as in comments page.

## Running

First `npm install` to grab all the necessary dependencies. 

Then run `npm start` and open <localhost:7770> in your browser.

## Production Build

Run `npm run build` to create a distro folder and a bundle.js file.

## Url

[Click here](https://darshan-reduxstagram.herokuapp.com/) to view the application. This app is hosted on heroku.

## Future Work

This app currently deals with static data of instagram, next step would be to handle dynamic and live data.
